const mongoose = require('mongoose')


const truckSchema = new mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    assigned_to: {
        type: mongoose.Schema.Types.ObjectId
    },
    type: {
        type: String,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
        required: true
    },
    status: {
        type: String,
        default: 'IS'
    },
    created_date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Truck', truckSchema);