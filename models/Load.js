const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    assigned_to: {
        type: mongoose.Schema.Types.ObjectId
    },
    status: {
        type: String, 
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
        default: 'NEW'
    },
    state: {
        type: String,
        default: ''
    },
    name: {
        type: String,
        required: true
    },
    payload: {
        type: Number,
        required: true
    },
    pickup_address: {
        type: String,
        required: true,
        max: 255
    },
    delivery_address: {
        type: String,
        required: true,
        max: 255
    },
    dimensions: {
        width: {
            type: Number,
            required: true
        },
        length: {
            type: Number,
            required: true
        },
        height: {
            type: Number,
            required: true
        }
    },
    logs: { 
        type: Array,
        message: { type: String },
        time: { 
            type: String,
            default: Date.now
        }
    },
    created_date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Load', loadSchema);