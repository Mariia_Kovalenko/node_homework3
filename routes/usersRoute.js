const express = require('express');
const router = express.Router();
const verifyJWT = require('../middlewares/verifyToken');

const {getUser, deleteUser, updateUser} = require('../controllers/usersController')

// get current user
router.get('/me', verifyJWT, getUser);

// delete current user
router.delete('/me', verifyJWT, deleteUser)

// // change password
router.patch('/me/password', verifyJWT, updateUser)

module.exports = router;