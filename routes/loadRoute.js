const express = require('express');
const router = express.Router();
const verifyJWT = require('../middlewares/verifyToken');

const {
    createLoad,
    getLoads,
    updateNewLoads,
    deleteNewLoads,
    postLoad,
    getActiveLoad,
    changeLoadState,
    getShippingInfo
} = require('../controllers/loadsController');

router.post('/', verifyJWT, createLoad);

router.get('/', verifyJWT, getLoads);

router.get('/active', verifyJWT, getActiveLoad);

router.patch('/active/state', verifyJWT, changeLoadState);

router.put('/:id', verifyJWT, updateNewLoads);

router.delete('/:id', verifyJWT, deleteNewLoads);

router.post('/:id/post', verifyJWT, postLoad);

router.get('/:id/shipping_info', verifyJWT, getShippingInfo);

module.exports = router;