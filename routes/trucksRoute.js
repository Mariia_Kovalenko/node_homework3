const express = require('express');
const router = express.Router();
const verifyJWT = require('../middlewares/verifyToken');

const {
    createTruck, 
    getTrucks, 
    getTruckById, 
    deleteTruckById, 
    updateTruckById,
    assignTruck
} = require('../controllers/trucksController');

router.post('/', verifyJWT, createTruck);

router.get('/', verifyJWT, getTrucks);

router.get('/:id', verifyJWT, getTruckById);

router.delete('/:id', verifyJWT, deleteTruckById);

router.put('/:id', verifyJWT, updateTruckById);

router.post('/:id/assign', verifyJWT, assignTruck);

module.exports = router;