# Description
This is a Node JS + React UBER-like CRUD API application. 
In this app you can register as shipper or driver. As shipper, you can create loads and post them for drivers to 
deliver. As driver, you can create and assign trucks, as well as deliver user loads, assigned to you.
Any user can login, logout and manage his/her profile info. 
In case you forgot your password, you can restore it by clicking the corresponding link.
You will recieve one-time password restoration link on your email, which will be valid for 10 minutes 
and will redirect you to password restoration form.
Pay attention than this link will be sent from a dummy email dummyjohn156@gmail.com. Therefore, you should carefully 
check your spam. 

## Run the application
Install the dependencies for both server and client side using
## `npm install`

To run the application, you need to run client and server sides separately. 

To launch the server in production mode run (in node_homework3 directory)
## `npm start`
To launch the server in development mode run (in node_homework3 directory)
## `npm run dev`
The server will start on [http://localhost:8080](http://localhost:8080)

To run React application, move to client directory:
## `cd client`

Then, to start the app in development mode, run
## `npm start`

The app will be launched on [http://localhost:3000](http://localhost:3000)