// validation
const Joi = require('@hapi/joi');

const loadValidation = (data) => {
    const schema = Joi.object().keys({
        name: Joi.string().required(),
        payload: Joi.number().required(),
        pickup_address: Joi.string().max(255).required(),
        delivery_address: Joi.string().max(255).required(),
        dimensions: {
            width: Joi.number().required(),
            length: Joi.number().required(),
            height: Joi.number().required()
        }
    });
    return schema.validate(data)
}

module.exports = {loadValidation};