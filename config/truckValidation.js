// validation
const Joi = require('@hapi/joi');

const truckValidation = (data) => {
    const schema = Joi.object().keys({
        type: Joi.string().required()
    });
    return schema.validate(data)
}

module.exports = {truckValidation}