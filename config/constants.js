const loadStates = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery'
];

const truckStates = {
    inServise: 'IS',
    onLoad: 'OL'
}

module.exports = {
    loadStates,
    truckStates
}