module.exports = function validateLoad(truckType, width, length, height, payload) {
    switch (truckType) {
        case 'SPRINTER':
            if (width < 300 && length < 250 && height < 170 && payload < 1700) {
                return true;
            } 
            return false
        case 'SMALL STRAIGHT':
            if (width < 500 && length < 250 && height < 170 && payload < 2500) {
                return true;
            } 
            return true;
        case 'LARGE STRAIGHT':
            if (width < 700 && length < 350 && height < 200 && payload < 4000) {
                return true;
            } 
            return true;
    
        default:
            return false
    }
}