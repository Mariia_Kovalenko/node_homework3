import axios from 'axios';


const BASE_URL = 'http://localhost:8080/api/';
class ApiService {

    async registerUser(params) {
        // console.log('register');
        try {
            let response = axios.post(BASE_URL + 'auth/register', {
                name: params.name,
                email: params.email,
                password: params.password,
                role: params.role
            })
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async loginUser(params) {
        // console.log('register');
        try {
            let response = axios.post(BASE_URL + 'auth/login', {
                email: params.email,
                password: params.password,
            })
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async logoutUser(token) {
        // console.log('register');
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.post(BASE_URL + 'auth/logout')
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async deleteProfile(token) {
        try {
            let response = axios.delete(BASE_URL + 'users/me', {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error.message);
        }
    }

    async getProfileInfo(token) {
        try {
            let response = axios.get(BASE_URL + 'users/me', {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error.message);
        }
    }

    async changePassword(params, token) {
        try {
            let response = axios.patch(BASE_URL + 'users/me/password', {
                oldPassword: params.oldPassword,
                newPassword: params.newPassword
            }, {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            });
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async restorePassword(params) {
        try {
            let response = axios.post(BASE_URL + 'auth/forgot_password', {
                email: params.email
            })
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async createLoad(params, token) {
        // console.log(params);
        try {
            let response = axios.post(BASE_URL + 'loads/', {
                ...params
            }, {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async updateLoad(id, params, token) {
        try {
            let response = axios.put(BASE_URL + 'loads/' + id, {
                ...params
            }, {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async getLoads(params, token) {
        // console.log(params);
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.get(BASE_URL + 'loads/', {
                ...params
            })
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async postLoad(id, token) {
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.post(BASE_URL + 'loads/' + id + '/post')
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async deleteLoad(id, token) {
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.delete(BASE_URL + 'loads/' + id)
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async getShippingInfo(id, token) {
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.get(BASE_URL + 'loads/' + id + '/shipping_info')
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async createTruck(type, token) {
        try {
            let response = axios.post(BASE_URL + 'trucks/', {
                type: type
            }, {
                headers: {
                    "authorization": `Bearer ${token}`
                }
            })
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async getTrucks(token) {
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.get(BASE_URL + 'trucks/')
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async deleteTruck(id, token) {
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.delete(BASE_URL + 'trucks/' + id)
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async assignTruck(id, token) {
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.post(BASE_URL + 'trucks/' + id + '/assign')
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async updateTruck(id, type, token) {
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.put(BASE_URL + 'trucks/' + id, {
                type
            })
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async getActiveLoad(token) {
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.get(BASE_URL + 'loads/active')
            return response
        } catch (error) {
            console.log(error);
        }
    }

    async changeActiveLoadState(token) {
        try {
            axios.defaults.headers.authorization = `Bearer ${token}`;
            let response = axios.patch(BASE_URL + 'loads/active/state')
            return response
        } catch (error) {
            console.log(error);
        }
    }
}

export default ApiService;