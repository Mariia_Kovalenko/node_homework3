import NewLoad from "../NewLoad/NewLoad";
import { useState, useEffect } from "react";
import ApiService from "../../services/apiService";
import Select from "react-select";

const Loads = (props) => {

    const authToken = props.authToken;

    const [loads, setLoads] = useState([]);
    const [editedLoad, setEditedLoad] = useState(null);
    const [editLoadModal, setEditModal] = useState(false);
    const [statusFilter, setStatusFilter] = useState('');
    const [offsetFilter, setOffsetFilter] = useState('');
    const [limitFilter, setLimitFilter] = useState('');

    // console.log(loads);

    const apiService = new ApiService();

    const filterOptions = [
        {value: 'ALL', label: "All"},
        {value:'NEW', label: "New"},
        {value: 'ASSIGNED', label: "Assigned"}
    ]

    useEffect(() => {
        getLoads({});
    }, []);

    const setOffset = (e) => {
        setOffsetFilter(e.target.value);
    }
    const setLimit = (e) => {
        setLimitFilter(e.target.value);
    }

    // const filterLoads = (e) => {
    //     // for some reason pagination as server request does not work from client
    //     e.preventDefault();
    //     // let params = {}
    //     let filteredLoads = [];
    //     if (statusFilter) {
    //         if (statusFilter !== 'ALL') {
    //             filteredLoads = loads.filter(item => item.status === statusFilter);
    //             setLoads(filteredLoads);
    //             // params = {status: statusFilter.value};
    //         }
    //     } 
    //     if (offsetFilter) {
    //         filteredLoads = loads.slice(offsetFilter);
    //         setLoads(filteredLoads);
    //         // params = {...params, offset: offsetFilter};
    //     }
    //     if (limitFilter) {
    //         filteredLoads = loads.slice(0, limitFilter);
    //         setLoads(filteredLoads);
    //         // params = {...params, limit: limitFilter};
    //     }
    //     // console.log('filters:', params);
    // }

    const getLoads = (params) => {
        apiService.getLoads({
            ...params
        }, authToken).then(res => {
            console.log(res);
            setLoads(res.data.loads);
        }).catch(error => {
            console.log(error);
        })
    }

    const createLoad = (params) => {
        apiService.createLoad({
            name: params.name,
            payload: params.payload,
            pickup_address: params.pickup_address,
            delivery_address: params.delivery_address,
            dimensions: {
                width: params.width,
                length: params.length,
                height: params.height
            }
        }, authToken).then(res => {
            console.log(res);
            alert(res.data.message);
            getLoads()
        }).catch(error => {
            console.log(error);
        })
    }

    const updateLoad = (params) => {
        apiService.updateLoad(editedLoad, {
            ...params
        }, authToken).then(res => {
            console.log(res);
            alert(res.data.message);
            setEditModal(false);
            getLoads();
        }).catch(error => {
            console.log(error);
        })
    }

    const postLoad = (id) => {
        apiService.postLoad(id, authToken)
            .then(res => {
                console.log(res);
                alert(res.data.message);
                getLoads()
            }).catch(error => {
                console.log(error);
            })
    }

    const deleteLoad = (id) => {
        apiService.deleteLoad(id, authToken)
            .then(res => {
                console.log(res);
                alert(res.data.message);
                getLoads()
            }).catch(error => {
                console.log(error);
                alert(error.response.data.message);
            })
    }

    const editLoad = (item) => {
        if (item.status !== 'NEW') {
            alert('You can only update new loads');
            return;
        }
        setEditedLoad(item._id);
        setEditModal(true);
        window.scrollTo(0, 0);
    }

    function renderLoads(loads) {
        if (!loads.length) {
            return (
                <div>
                    You have not yet created any loads
                </div>
            )
        }

        const items = loads.map(item => {
            return (
                <div key={item._id} className="load">
                    <div className="load__id">
                        #<span>{item._id}</span>
                    </div>
                    <div className="load__date">
                        {item.created_date.slice(0, 10)}
                    </div>
                    <div className="load__detail">
                        Name: <span>{item.name}</span>
                    </div>
                    <div className="load__status">
                    {item.status === 'ASSIGNED' ? 'ASGN' : item.status}
                    </div>
                    <div className="load__detail">
                        Pickup Address: <span>{item.pickup_address}</span>
                    </div>
                    <div className="load__detail">
                        Delivery Address: <span>{item.delivery_address}</span>
                    </div>
                    <div className="load__detail">
                        Weight: <span>{item.payload}</span>
                    </div>
                    <div className="load__detail">
                        Dimentions: <span>{item.dimensions.width}</span> x <span>{item.dimensions.length}</span> x <span>{item.dimensions.height}</span>
                    </div>
                    <div className="load__detail">
                        Last Update: <span>{item?.logs[item.logs.length -1]?.message}</span>
                    </div>
                    <div className="load__btns">
                        <button className="load__btn edit" onClick={() => editLoad(item)}>Edit</button>
                        <button className="load__btn post" onClick={() => postLoad(item._id)}>Post</button>
                        <button className="load__btn delete" onClick={() => deleteLoad(item._id)}>Delete</button>
                    </div>
                </div>
            )
        })

        return (
            <div className="loads__container">
                {items}
            </div>
        )
    }

    const content = renderLoads(loads);

    return(
        <>
            <h1 className="tab__heading">Loads</h1>
            <h2 className="title">Create Load</h2>
            <div className="create__load">
                <NewLoad type="create" createLoad={createLoad}/>
            </div>
            <h2 className="title">My Loads</h2>
            {/* <div className="subtitle">Filter Loads</div>
            <div className="load__filters">
                <div className="load__filter">
                    <Select
                    defaultValue={{value: 'Status', label: 'Status'}}
                    options={filterOptions}
                    styles={colourStyles}
                    onChange={setStatusFilter}
                    />
                </div>
                <div className="load__filter">
                    <input
                    type="number"
                    min="1"
                    max="50"
                    className="limit__filter"
                    placeholder="Offset"
                    onChange={setOffset}
                    />
                </div>
                <div className="load__filter">
                    <input
                    type="number"
                    min="1"
                    max="10"
                    className="limit__filter"
                    placeholder="Limit"
                    onChange={setLimit}
                    />
                </div>
                <button className="submit-btn" onClick={filterLoads}>Search</button>
            </div> */}
            {content}
            
            {editLoadModal ? 
                <div className="modal__bg">
                    <div className="modal">
                        <button className="close" onClick={() => setEditModal(false)}></button>
                        <h2 className="title">Update Load #{editedLoad}</h2>
                        <div className="create__load">
                            <NewLoad type="update" updateLoad={updateLoad}/>
                        </div>
                    </div>
                </div>
                : null
            }
        </>
    )
}

const colourStyles = {
    control: (styles) => (
        { 
            ...styles, 
            backgroundColor: "white", 
            width: '200px',
            height: '40px',
            border: 0,
            boxShadow: 'none',
            borderRadius: '10px',
        }),
    option: (styles, { isDisabled, isFocused }) => {
        return {
        ...styles,
        backgroundColor: isFocused ? "lightgrey" : "white",
        width: '200px',
        color: "#000",
        cursor: isDisabled ? "not-allowed" : "default",
        };
    },
}

export default Loads;