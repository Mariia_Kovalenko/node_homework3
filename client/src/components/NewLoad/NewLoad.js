import { useState } from "react";

const NewLoad = (props) => {
    const [name, setName] = useState('');
    const [payload, setPayload] = useState(0);
    const [pickupAddress, setPickupAddress] = useState('');
    const [deliveryAddress, setDeliveryAddress] = useState('');
    const [width, setWidth] = useState(0);
    const [length, setLength] = useState(0);
    const [height, setHeight] = useState(0);

    const type = props.type;


    const setLoadName = (e) => {
        setName(e.target.value);
    } 
    const setLoadPayload = (e) => {
        setPayload(e.target.value);
    } 
    const setLoadPickupAddress = (e) => {
        setPickupAddress(e.target.value);
    } 
    const setLoadDeliveryAddress = (e) => {
        setDeliveryAddress(e.target.value);
    } 
    const setLoadWidth = (e) => {
        setWidth(e.target.value);
    } 
    const setLoadLength = (e) => {
        setLength(e.target.value);
    } 
    const setLoadHeight = (e) => {
        setHeight(e.target.value);
    } 

    const createLoad = (e) => {
        e.preventDefault();
        props.createLoad({
            name,
            payload,
            pickup_address: pickupAddress,
            delivery_address: deliveryAddress,
            width,
            length,
            height
        })
        setName('');
        setPayload(0);
        setPickupAddress('');
        setDeliveryAddress('');
        setHeight(0);
        setWidth(0);
        setLength(0)
    }

    const updateLoad = (e) => {
        e.preventDefault();
        let params = {};
        let dimensions = {};
        if (name) {
            params = {...params, name};
        }
        if (payload) {
            params = {...params, payload};
        }
        if (pickupAddress) {
            params = {...params, pickup_address: pickupAddress};
        }
        if (deliveryAddress) {
            params = {...params, delivery_address: deliveryAddress};
        }
        if (width) {
            dimensions = {...dimensions, width};
        }
        if (length) {
            dimensions = {...dimensions, length};
        }
        if (height) {
            dimensions = {...dimensions, height};
        }
        if (Object.keys(dimensions).length) {
            props.updateLoad({...params, dimensions: {...dimensions}});
        } else {
            props.updateLoad({...params});
        }
        setName('');
        setPayload(0);
        setPickupAddress('');
        setDeliveryAddress('');
        setHeight(0);
        setWidth(0);
        setLength(0)
    }

    return (
        <>
            <form className="load__form">
                <fieldset>
                    <label htmlFor="name">Name</label>
                    <input
                    className="input"
                    type="text"
                    required
                    name="name"
                    value={name}
                    onChange={setLoadName}
                    >
                    </input>
                </fieldset>
                <fieldset>
                    <label htmlFor="payload">Weight</label>
                    <input
                    className="input"
                    type="number"
                    required
                    name="payload"
                    value={payload}
                    onChange={setLoadPayload}
                    >
                    </input>
                </fieldset>
                <fieldset>
                    <label htmlFor="pickup">Pickup Address</label>
                    <input
                    className="input"
                    type="text"
                    required
                    name="pickup"
                    value={pickupAddress}
                    onChange={setLoadPickupAddress}
                    >
                    </input>
                </fieldset>
                <fieldset>
                    <label htmlFor="delivery">Delivery Address</label>
                    <input
                    className="input"
                    type="text"
                    required
                    name="delivery"
                    value={deliveryAddress}
                    onChange={setLoadDeliveryAddress}
                    >
                    </input>
                </fieldset>
                <fieldset>
                    <label htmlFor="width">Width</label>
                    <input
                    className="input"
                    type="number"
                    required
                    name="width"
                    value={width}
                    onChange={setLoadWidth}
                    >
                    </input>
                </fieldset>
                <fieldset>
                    <label htmlFor="length">Length</label>
                    <input
                    className="input"
                    type="number"
                    required
                    name="length"
                    value={length}
                    onChange={setLoadLength}
                    >
                    </input>
                </fieldset>
                <fieldset>
                    <label htmlFor="length">Height</label>
                    <input
                    className="input"
                    type="number"
                    required
                    name="height"
                    value={height}
                    onChange={setLoadHeight}
                    >
                    </input>
                </fieldset>
                <fieldset>
                    <button 
                    className="submit-btn"
                    onClick={type === "create" ? createLoad : updateLoad}
                    > {type === "create" ? "Create Load" : "Update Load"}</button>
                </fieldset>
                
            </form>
        </>
    )
}

export default NewLoad;