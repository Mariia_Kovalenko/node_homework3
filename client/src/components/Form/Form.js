import {useState} from 'react';
import ApiService from '../../services/apiService';

const Form = (props) => {
    const [type, setType] = useState(props.type);
    const [role, setRole] = useState('');

    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    
    const apiService = new ApiService();

    const formName = type === 'login' ? 'Log In' : type === 'register' ? 'Register' : 'Restore Password';
    const displayRegisterFields = type === 'login' ? 'none' : 'block';

    const displayLoginFields = type === 'register' ? 'none' : 'block';

    let driverBtnClassNames = 'user__role ';
    let shipperBtnClassNames = 'user__role ';

    if (role === 'DRIVER') {
        driverBtnClassNames += 'role__selected';
    }
    if (role === 'SHIPPER') {
        shipperBtnClassNames += 'role__selected';
    }

    const changeFormType = (e, type) => {
        e.preventDefault();
        // console.log(type);
        setType(type)
    }

    const setUserRole = (e) => {
        e.preventDefault();
        setRole(e.target.value);
    }

    const setUserName = (event) => {
        event.preventDefault();
        setName(event.target.value);
    }

    const setUserEmail = (event) => {
        event.preventDefault();
        setEmail(event.target.value);
    }

    const setUserPass = (event) => {
        event.preventDefault();
        setPassword(event.target.value);
    }

    const authUser = (e) => {
        e.preventDefault();
        if (type === 'login') {
            loginUser();
        }
        if (type === 'register') {
            if (!role) {
                alert('Please select role');
                return;
            }
            registerUser();
        }
        if (type == 'restorepass') {
            restorePassword();
        }
    }

    const loginUser = () => {
        apiService.loginUser({
            email: email,
            password: password
        }).then(res => {
            console.log(res);
            const token = res.data.jwt_token;
            // save token to localStorage
            localStorage.setItem("token", token);
            props.setUser(token);
        }).catch(error => {
            console.log(error);
            if (error.response.data.message) {
                alert(error.response.data.message);
            }
            else if (error.response.data) {
                alert(error.response.data);
            }
        })
    }

    const registerUser = () => {
        apiService.registerUser({
            name: name,
            email: email,
            password: password,
            role: role
        }).then(res => {
            // console.log(res);
            setType('login');
            setEmail('');
            setPassword('');
        }).catch(error => {
            console.log(error);
            alert(error.response.data);
        })
    }

    const restorePassword = () => {
        apiService.restorePassword({
            email: email
        }).then(res => {
            console.log(res);
            alert('Check your email for restore link')
            setEmail('');
            setType('login');
        }).catch(error => {
            console.log(error);
        })
    }

    return (
        <div className='form__container'>
            <form className="form">
                <div className='logo'>
                    Thunder<span>Storm</span>
                </div>
                <h2 className='form__name'>{formName}</h2>

                <fieldset style={{display: type === 'register' ? 'block' : 'none'}}>
                    <label htmlFor="name">Name</label>
                    <input 
                    required 
                    className='user__input' 
                    type="text" 
                    name="name" 
                    placeholder="Enter name"
                    value={name}
                    onChange={setUserName}
                    ></input>
                </fieldset>

                <fieldset>
                    <label htmlFor="email">Email</label>
                    <input 
                    required 
                    className='user__input' 
                    type="email" 
                    name="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={setUserEmail}
                    ></input>
                </fieldset>
                
                <fieldset style={{display: type === 'restorepass' ? 'none' : 'block'}}>
                    <label htmlFor="password">Password</label>
                    <input 
                    required 
                    className='user__input' 
                    type="password" 
                    name="password" 
                    placeholder="Enter password"
                    value={password}
                    onChange={setUserPass}
                    ></input>
                </fieldset>

                <fieldset style={{display: type === 'register' ? 'block' : 'none'}}>
                    <label>Choose your role</label>
                    <div className='user__roles'>
                        <button value="DRIVER" className={driverBtnClassNames} onClick={setUserRole}>Driver</button>
                        <button value="SHIPPER" className={shipperBtnClassNames} onClick={setUserRole}>Shipper</button>
                    </div>
                </fieldset>

                <button className='form__button' onClick={authUser}>{formName}</button>
                
                <div style={{display: displayLoginFields}}>
                    <div className='form__link' >
                        <p>Do not have an account?</p>
                        <button onClick={(e) => changeFormType(e, 'register')}>Register</button>
                    </div>
                </div>

                <div style={{display: displayRegisterFields}}>
                    <div className='form__link'>
                        <p>Already have an account?</p>
                        <button onClick={(e) => changeFormType(e, 'login')}>Log In</button>
                    </div>
                </div>

                <div className='form__link'>
                    <button onClick={(e) => changeFormType(e, 'restorepass')} 
                    style={{display: type === 'restorepass' || type === 'register' ? 'none' : 'block'}}>Forgot password</button>
                </div>
            </form>
        </div>
    )
}

export default Form;