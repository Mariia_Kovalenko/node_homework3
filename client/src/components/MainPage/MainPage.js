import { useState, useEffect } from "react";
import ApiService from "../../services/apiService";
import TabNavItem from "../TabNavItem/TabNavItem";
import TabContent from '../TabContent/TabContent';
import Profile from '../Profile/Profile';
import Loads from "../Loads/Loads";
import AssignedLoad from "../AssignedLoad/AssignedLoad";
import Trucks from "../Trucks/Trucks";
import ActiveLoad from "../ActiveLoad/ActiveLoad";

const MainPage = (props) => {
    const [activeTab, setActiveTab] = useState("tab1");
    const [navClass, setNavClass] = useState("nav hidenav");
    const [user, setUser] = useState({name: "No user"});

    const [assignedLoad, setAssignedLoad] = useState(null);

    const userRole = props.userRole;
    let authToken = props.authToken;
    // console.log(authToken);

    if (!authToken ) {
        authToken = localStorage.getItem("token");
        // console.log(authToken === 'undefined');
    }

    const apiService = new ApiService();

    useEffect(() => {
        getUserInfo();
    }, []);

    const hidenav = (e) => {
        e.preventDefault();
        if (navClass === "nav hidenav") {
            setNavClass("nav")
        } else {
            setNavClass("nav hidenav")
        }
    }

    const getUserInfo = () => {
        console.log('user info request');
        apiService.getProfileInfo(authToken)
            .then(res => {
                // console.log(res);
                setUser(res.data.user);
                props.setUserRole(res.data.user.role);
            }).catch(error => {
                console.log(error);
            })
    }

    const changePassword = (oldPassword, newPassword) => {
        if (!(oldPassword && newPassword)) {
            alert('Enter credentials')
            return;
        }
        apiService.changePassword({
            oldPassword: oldPassword,
            newPassword: newPassword
        }, authToken)
            .then(res => {
                console.log(res);
                
            }).catch(error => {
                console.log(error);
                alert(error.response.data.message);
            })
    }

    const logout = () => {
        apiService.logoutUser(authToken)
            .then(res => {
                // console.log(res);
                props.setUserLoggedIn(false);
            }).catch(error => {
                console.log(error);
            })
    }

    const deleteProfile = () => {
        apiService.deleteProfile(authToken)
        .then(res => {
            console.log(res);
            props.setUserLoggedIn(false);
        }).catch(error => {
            console.log(error);
        })
    }

    return (
        <div className="container">
            <header className="header">
                <div className="header__inner">
                    <button className={navClass === "nav" ? "burger__menu cross" : "burger__menu"} onClick={hidenav}>
                        <span></span>
                        <span></span>
                    </button>
                    <a className="logo">Thunder<span>Storm</span></a>
                </div>
                
                <button className="logout-btn" onClick={logout}>Log Out</button>
            </header>
            <div className="content">
                <nav className={navClass}>
                    <ul className="nav__list">
                        <TabNavItem id="tab1" title="Profile" activeTab={activeTab} setActiveTab={setActiveTab} hidenav={hidenav}></TabNavItem>
                        <TabNavItem id="tab2" title={userRole === 'DRIVER' ? "Trucks" : "Loads"} activeTab={activeTab} setActiveTab={setActiveTab} hidenav={hidenav}></TabNavItem>
                        <TabNavItem id="tab3" title="Assigned Load" activeTab={activeTab} setActiveTab={setActiveTab} hidenav={hidenav}></TabNavItem>
                    </ul>
                </nav>
                <main className="main">
                    <TabContent id="tab1" activeTab={activeTab}>
                        <Profile user={user} changePassword={changePassword} deleteProfile={deleteProfile}></Profile>
                    </TabContent>
                    <TabContent id="tab2"  activeTab={activeTab}>
                        { userRole === 'DRIVER' ?
                            <Trucks authToken={authToken} setAssignedLoad={setAssignedLoad}/> :
                            <Loads authToken={authToken} setAssignedLoad={setAssignedLoad}/>}
                    </TabContent>
                    <TabContent id="tab3"  activeTab={activeTab}>
                        { userRole === 'DRIVER' ?
                            <ActiveLoad authToken={authToken}/> :
                            <AssignedLoad authToken={authToken}/>
                        }
                    </TabContent>
                </main>
            </div>
        </div>
    )
}

export default MainPage;