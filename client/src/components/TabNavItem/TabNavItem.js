const TabNavItem = ({ id, title, activeTab, setActiveTab, hidenav }) => {

    const handleClick = (e) => {
        hidenav(e);
        setActiveTab(id);
    };

    return (
        <li onClick={handleClick} className={activeTab === id ? "tabs__name active" : "tabs__name"}>
            <button>{ title }</button>
        </li>
    );
};
export default TabNavItem;