import { useState } from "react";

const Profile = (props) => {

    const {_id, name, email, role} = props.user;
    // console.log(name);

    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');

    const changeOldPassword = (event) => {
        event.preventDefault();
        setOldPassword(event.target.value);
    }
    
    const changeNewPassword = (event) => {
        event.preventDefault();
        setNewPassword(event.target.value);
    }

    const changePassword = () => {
        props.changePassword(oldPassword, newPassword);
        setOldPassword('');
        setNewPassword('');
    }

    const deleteProfile = (e) => {
        e.preventDefault();
        const submit = window.confirm('Are you sure you want to delete your profile?');
        if (submit) {
            props.deleteProfile();
        }
    }

    return (
        <>
            <h1 className="tab__heading">Profile</h1>
            <div className="profile__info">
                <div className="profile__image">
                    <img src="images/account.svg" width="100px" height="100px"></img>
                    <div className="profile__name">{name}</div>
                </div>
                <div className="profile__details">
                    <div className="profile__name">UserID: <span>{_id}</span></div>
                    <div className="profile__name">Email: <span>{email}</span></div>
                    <div className="profile__name">Role: <span>{role}</span></div>
                </div>
                <button className="submit-btn" onClick={deleteProfile}>Delete Profile</button>
            </div>

            <div>
                <h2 className="title">Edit profile</h2>
                <h3 className="subtitle">Change password</h3>
                <div>
                    <fieldset>
                    <label>Old password</label>
                    <input 
                    type="password" 
                    className="input"
                    value={oldPassword}
                    onChange={changeOldPassword}></input>
                    </fieldset>
                    
                    <fieldset>
                    <label>New password</label>
                    <input 
                    type="password" 
                    className="input"
                    value={newPassword}
                    onChange={changeNewPassword}></input>
                    </fieldset>
                    

                    <button 
                    className="submit-btn"
                    onClick={changePassword}
                    >Change Password</button>
                </div>
            </div>
        </>
    )
}

export default Profile;