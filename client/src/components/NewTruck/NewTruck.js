import { useState } from "react";

const NewTruck = (props) => {
    const [type, setType] = useState('');

    const setTruckType = (e) => {
        e.preventDefault();
        setType(e.target.value);
    }

    const createTruck = (e) => {
        e.preventDefault();
        console.log(type);
        props.createTruck(type.toUpperCase())
        setType('');
    }

    return (
        <form className="load__form">
            <div className="btns-container">
                <button
                className={type === 'sprinter' ? 'truck-btn active' : 'truck-btn'}
                value="sprinter"
                onClick={setTruckType}
                > Sprinter
                </button>

                <button
                className={type === 'large straight' ? 'truck-btn active' : 'truck-btn'}
                value="large straight"
                onClick={setTruckType}
                > Large Straight
                </button>

                <button
                className={type === 'small straight' ? 'truck-btn active' : 'truck-btn'}
                value="small straight"
                onClick={setTruckType}
                > Small Straight
                </button>
                </div>
                <fieldset>
                    <button 
                    className="submit-btn"
                    onClick={createTruck}
                    >Create Truck</button>
                </fieldset>
        </form>
    )
}

export default NewTruck;