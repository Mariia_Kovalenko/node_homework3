import Form from '../Form/Form';
import {useState} from 'react';
import MainPage from '../MainPage/MainPage';

const App = () => {

  const [isUserLoggedIn, setUserLoggedIn] = useState(false); // false default
  const [authToken, setAuthToken] = useState('');

  const [userRole, setUserRole] = useState(''); // '' default

  const setUser = (token) => {
    // console.log('token:', token);
    setAuthToken(token);
    setUserLoggedIn(true);
  }

  const content = isUserLoggedIn ? <MainPage setUserLoggedIn={setUserLoggedIn} userRole={userRole} setUserRole={setUserRole} authToken={authToken}/> : <Form type={"login"} setUser={setUser}/>

  return (
    <div className="App">
      {content}
    </div>
  );
}



export default App;
