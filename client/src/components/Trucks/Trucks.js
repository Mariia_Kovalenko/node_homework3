import NewTruck from "../NewTruck/NewTruck";
import ApiService from "../../services/apiService";
import { useState, useEffect } from "react";

const Trucks = (props) => {
    
    const authToken = props.authToken;
    const apiService = new ApiService();

    const [trucks, setTrucks] = useState([]);
    const [assignedTruck, setAssignedTruck] = useState('');
    const [editedTruck, setEditedTruck] = useState(null);
    const [type, setType] = useState('');

    const setTruckType = (e) => {
        e.preventDefault();
        setType(e.target.value);
    }

    useEffect(() => {
        getTrucks()
    }, [])

    const createTruck = (type) => {
        apiService.createTruck(type, authToken)
            .then(res => {
                console.log(res);
                alert(res.data.message);
                getTrucks();
            }).catch(error => {
                console.log(error);
            })
    }

    const getTrucks = () => {
        apiService.getTrucks(authToken)
            .then(res => {
                console.log(res);
                setTrucks(res.data.trucks);
            }).catch(error => {
                console.log(error);
            })
    }

    const deleteTruck = (id) => {
        apiService.deleteTruck(id, authToken)
        .then(res => {
            console.log(res);
            alert(res.data.message);
            getTrucks()
        }).catch(error => {
            console.log(error);
            alert(error.response.data.message);
        })
    }

    const assignTruck = (id) => {
        apiService.assignTruck(id, authToken)
        .then(res => {
            console.log(res);
            alert(res.data.message);
            setAssignedTruck(id);
            getTrucks();
        }).catch(error => {
            console.log(error);
            alert(error.response.data.message);
        })
    }

    const updateTruck = () => {
        apiService.updateTruck(editedTruck, type.toUpperCase(), authToken)
            .then(res => {
                console.log(res);
                alert(res.data.message);
                setType('');
                setEditedTruck('');
                getTrucks()
            }).catch(error => {
                console.log(error);
                if (error.response.data.message) {
                    alert(error.response.data.message);
                }
            })
    }

    const closeEdit = () => {
        setEditedTruck(null);
        setType('');
    }

    function renderEditBlock() {
        return (
            <div className="edit__truck">
                <div className="subtitle">Select new truck type</div>
                <button className="close small-close" onClick={closeEdit}></button>
                <div className="btns-container">
                    <button
                    className={type === 'sprinter' ? 'truck-btn small active' : 'truck-btn small'}
                    value="sprinter"
                    onClick={setTruckType}
                    > Sprinter
                    </button>

                    <button
                    className={type === 'large straight' ? 'truck-btn small active' : 'truck-btn small'}
                    value="large straight"
                    onClick={setTruckType}
                    > Large Straight
                    </button>

                    <button
                    className={type === 'small straight' ? 'truck-btn small active' : 'truck-btn small'}
                    value="small straight"
                    onClick={setTruckType}
                    > Small Straight
                    </button>
                </div>
                    <button 
                    className="submit-btn wide"
                    onClick={updateTruck}
                    >Update Truck</button>
            </div>
        )
    }

    function renderTrucks(trucks) {
        if (!trucks.length) {
            return (
                <div>
                    You have not yet created any trucks
                </div>
            )
        }

        const items = trucks.map(item => {
            return (
                <div key={item._id} className="load">
                    <div className="load__id">
                        #<span>{item._id}</span>
                    </div>
                    <div className="load__date">
                        {item.created_date.slice(0, 10)}
                    </div>
                    <div className="load__detail">
                        Type: <span>{item.type}</span>
                    </div>
                    {
                        item.assigned_to ? 
                        <div className="load__detail">
                            ASSIGNED
                        </div> : null
                    }
                    <div className="load__status">
                        {item.status}
                    </div>
                    <div className="load__btns">
                        <button className="load__btn edit" onClick={() => setEditedTruck(item._id)}>Edit</button>
                        <button className="load__btn post" onClick={() => assignTruck(item._id)}>Assign</button>
                        <button className="load__btn delete" onClick={() => deleteTruck(item._id)}>Delete</button>
                    </div>
                    {editedTruck === item._id ? renderEditBlock() : null}
                </div>
            )
        })

        return (
            <div className="loads__container">
                {items}
            </div>
        )
    }

    const content = renderTrucks(trucks);
    const currentTruck = assignedTruck ? <View id={assignedTruck}/> : <div>You have not yet assigned any truck</div>

    return (
        <div>
            <h1 className="tab__heading">Trucks</h1>
            <h2 className="title">Create Truck</h2>
                <NewTruck authToken={authToken} createTruck={createTruck}/>
            <h2 className="title">My Trucks</h2>
            {content}
        </div>
    )
}

const View = ({id}) => {
    return (
        <div className="load">
            <div className="load__id">
                #<span>{id}</span>
            </div>
        </div>
    )
}

export default Trucks;