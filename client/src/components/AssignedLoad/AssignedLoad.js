import { useState, useEffect } from "react";
import ApiService from "../../services/apiService";

const AssignedLoad = (props) => {

    const [loadId, setLoadId] = useState('');
    const [assignedLoad, setAssignedLoad] = useState(null);
    const [assignedTruck, setAssignedTruck] = useState(null);
    const authToken= props.authToken;

    const apiService = new ApiService();

    const setLoad = (e) => {
        e.preventDefault();
        setLoadId(e.target.value);
    }

    const getShippingInfo = () => {
        apiService.getShippingInfo(loadId, authToken)
            .then(res => {
                console.log(res);
                setAssignedLoad(res.data.load);
                setAssignedTruck(res.data.truck);
                setLoadId('');

            }).catch(error => {
                console.log(error);
                // add this message to server
                alert('No assigned load with such id');
                setLoadId('');
            })
    }

    return (
        <div>
            <h1 className="tab__heading">Assigned Loads</h1>
            <h2 className="title">Find Load</h2>
            <fieldset>
                <label>Enter Load id</label>
                <input 
                className="input"
                value={loadId}
                onChange={setLoad}
                ></input>
            </fieldset>
            <button 
            className="submit-btn"
            onClick={getShippingInfo}
            >Find</button>
            <br/>
            <h2 className="title">Load Details</h2>
            <div className="loads__container">
                <View item={assignedLoad} truck={assignedTruck}/>
            </div>
        </div>
    )
}

const View = ({item, truck}) => {
    if (!item || !truck) {
        return (
            <div>Enter load id to view shipping info</div>
        )
    }
    // console.log(item.status);
    return (
        <div key={item._id} className="load">
            <div className="load__id">
                #<span>{item._id}</span>
            </div>
            <div className="load__date">
                {item.created_date.slice(0, 10)}
            </div>
            <div className="load__detail">
                Name: <span>{item.name}</span>
            </div>
            <div className="load__detail">
                State: <span>{item.state}</span>
            </div>
            <div className="load__status">
                {item.status === 'ASSIGNED' ? 'ASGN' : item.status}
            </div>
            <div className="load__detail">
                Pickup Address: <span>{item.pickup_address}</span>
            </div>
            <div className="load__detail">
                Delivery Address: <span>{item.delivery_address}</span>
            </div>
            <div className="load__detail">
                Weight: <span>{item.payload}</span>
            </div>
            <div className="load__detail">
                Dimentions: <span>{item.dimensions.width}</span> x <span>{item.dimensions.length}</span> x <span>{item.dimensions.height}</span>
            </div>
            <div className="load__detail">
                Truck: <span>{truck.type}</span>
            </div>
            <div className="load__detail">
                Last Update: <span>{item.logs[item.logs.length -1].message}</span>
            </div>
        </div>
    )
}

export default AssignedLoad;