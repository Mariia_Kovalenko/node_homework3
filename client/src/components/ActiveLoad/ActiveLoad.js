import { useState, useEffect } from "react";
import ApiService from "../../services/apiService";

const ActiveLoad = (props) => {

    const [activeLoad, setActiveLoad] = useState(null);
    const authToken= props.authToken;

    const apiService = new ApiService();

    useEffect(() => {
        getActiveLoad();
    }, [])

    const getActiveLoad = () => {
        apiService.getActiveLoad(authToken)
            .then(res => {
                console.log(res);
                setActiveLoad(res.data.load);
            }).catch(error => {
                console.log(error);
                alert('No assigned load found');
            })
    }

    const updateLoad = () => {
        apiService.changeActiveLoadState(authToken)
            .then(res => {
                console.log(res);
                if (res.data.message) {
                    alert(res.data.message);
                }
                getActiveLoad();
            }).catch(error => {
                console.log(error);
            })
    }
    
    return (
        <div>
            <h1 className="tab__heading">Active Load</h1>
            <div className="loads__container">
                <View item={activeLoad} updateLoad={updateLoad}/>
            </div>
        </div>
    )
}


const View = ({item, updateLoad}) => {
    if (!item) {
        return (
            <div>You have no active loads</div>
        )
    }
    // console.log(item.status);
    return (
        <div key={item._id} className="load">
            <div className="load__id">
                #<span>{item._id}</span>
            </div>
            <div className="load__date">
                {item.created_date.slice(0, 10)}
            </div>
            <div className="load__detail">
                Name: <span>{item.name}</span>
            </div>
            <div className="load__detail">
                State: <span>{item.state}</span>
            </div>
            <div className="load__status">
                {item.status === 'ASSIGNED' ? 'ASGN' : item.status}
            </div>
            <div className="load__detail">
                Pickup Address: <span>{item.pickup_address}</span>
            </div>
            <div className="load__detail">
                Delivery Address: <span>{item.delivery_address}</span>
            </div>
            <div className="load__detail">
                Weight: <span>{item.payload}</span>
            </div>
            <div className="load__detail">
                Dimentions: <span>{item.dimensions.width}</span> x <span>{item.dimensions.length}</span> x <span>{item.dimensions.height}</span>
            </div>
            <div className="load__detail">
                {/* add logging for shipped load to server */}
                Last Update: <span>{item?.logs[item.logs.length -1]?.message}</span>
            </div>
            <div className="load__btns">
                <button 
                className="load__btn edit"
                onClick={updateLoad}
                disabled={item.status === 'SHIPPED' ? true : false}
                >Update</button>
            </div>
        </div>
    )
}

export default ActiveLoad;