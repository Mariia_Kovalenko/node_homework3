const Load = require('../models/Load');
const User = require('../models/User');
const Truck = require('../models/Truck');
const validateLoad = require('../config/validateLoad');
const { loadValidation } = require('../config/loadValidation');
const {loadStates, truckStates} = require('../config/constants');


async function createLoad(req, res) {
    // data validation
    const {error} = loadValidation(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions
    } = req.body;

    const userId = req.user._id;

    // check user role
    const userRole = req.user.role;
    if (userRole !== 'SHIPPER') {
        return res.status(400).json({'message': 'Operation not allowed for current user'})
    }

    // create new Load
    const load = new Load({
        created_by: userId,
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
        logs: [
            {
                'message': 'Load created'
            }
        ]
    })

    try {
        const newLoad = await load.save();
        res.status(200).json({'message': 'Load created successfully'})
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

async function getLoads(req, res) {
    try {
        // check user role
        const userRole = req.user.role;
        if (userRole !== 'SHIPPER') {
            return res.status(400).json({'message': 'Operation not allowed for current user'})
        }

        const userId = req.user._id;
        let { limit, offset, status } = req.body;

        if (!limit || limit > 50) {
            limit = 10;
        }

        if (!offset) {
            offset = 0;
        }

        const userLoads = await Load.find({created_by: userId})
            .limit(+limit)
            .skip(+offset);

        const loads = status ? 
            userLoads.filter(load => load.status === status) :
            userLoads;

        res.status(200).json({'loads': loads})
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

async function getActiveLoad(req, res) {
    const userId = req.user._id;

    // check user role
    const userRole = req.user.role;
    if (userRole !== 'DRIVER') {
        return res.status(400).json({'message': 'Operation not allowed for current user'})
    }

    // get load assigned to current user
    try {
        const foundLoad = await Load.findOne({assigned_to: userId});
        // console.log(foundLoad);
        res.status(200).json({'load': foundLoad})
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

async function updateNewLoads(req, res) {
    const {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions
    } = req.body;

    const loadId = req.params.id;
    const userId = req.user._id;
    const userRole = req.user.role;

    if (userRole !== 'SHIPPER') {
        return res.status(400).json({'message': 'Operation not allowed for current user'})
    }

    if (!loadId) {
        return res.status(400).json({'message': 'No id specified'})
    }

    try {
        const load = await Load.findOne({_id: loadId, created_by: userId, status: 'NEW'});

        if (!load) {
            return res.status(400).json({'message': 'Cannot find load'});
        }

        const updatedLoad = await Load.findByIdAndUpdate(loadId, {
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions
        })
        res.status(200).json({'message': 'Load details changed successfully'});
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

async function deleteNewLoads(req, res) {
    const loadId = req.params.id;
    const userId = req.user._id;
    const userRole = req.user.role;

    if (userRole !== 'SHIPPER') {
        return res.status(400).json({'message': 'Operation not allowed for current user'})
    }

    if (!loadId) {
        return res.status(400).json({'message': 'No id specified'})
    }

    try {
        const load = await Load.findOne({_id: loadId, created_by: userId});
        if (load.status !== 'NEW') {
            return res.status(400).json({'message': 'You can only delete new loads'})
        }
        const deleteLoad = await Load.findOneAndDelete({_id: loadId, created_by: userId});
        res.status(200).json({'message': 'Load deleted successfully'});
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

async function postLoad(req, res) {
    const loadId = req.params.id;
    const userRole = req.user.role;

    if (userRole !== 'SHIPPER') {
        return res.status(400).json({'message': 'Operation not allowed for current user'})
    }

    if (!loadId) {
        return res.status(400).json({'message': 'No id specified'})
    }

    try {
        // check if load is already assigned
        const load = await Load.findOne({_id: loadId});
        if (!load) {
            return res.status(400).json({'message': 'No load with such id found'})
        }
        if (load.assigned_to) {
            return res.json({'message': 'Load already assigned'})
        }

        const logs = load.logs;

        // change load status from NEW to POSTED
        const loadPosted = await load.updateOne({
            $set: {
                status: 'POSTED'
            },
            $push: {
                logs: {
                    message: 'Load status changed to POSTED'
                }
            }
        })

        // find trucks with assigned driver and status "IS"
        const trucks = await Truck.find({assigned_to: {$exists: true}, status: truckStates.inServise});
        const {width, length, height} = load.dimensions;
        const payload = load.payload;
        let truckFound;

        for (let i = 0; i < trucks.length; i++) {
            const loadFits = validateLoad(trucks[i].type, width, length, height, payload);
            if (loadFits) {
                truckFound = trucks[i];
                break;
            }
        }

        // if truck not found, roll back to 'NEW' status
        if (!truckFound) {
            const loadNotAssigned = await load.updateOne({
                $set: {
                    status: 'NEW'
                },
                $push: {
                    logs: {
                        message: 'Load status changed to NEW'
                    }
                }
            })
            return res.json({
                'message': 'Load was not assigned successfully',
                'driver_found': false
            })
        }

        // if truck found, update truck status
        const updatedTruck = await Truck.findByIdAndUpdate(truckFound._id, {status: truckStates.onLoad});

        const loadAssigned = await load.updateOne({
            $set: {
                assigned_to: truckFound.assigned_to, 
                status: 'ASSIGNED', 
                state: loadStates[0]
            },
            $push: {
                logs: {
                    message: `Load assigned to driver with id ${truckFound.assigned_to}`
                }
            }
        });

        res.status(200).json({
            'message': 'Load posted successfully',
            'driver_found': true
        })
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

async function changeLoadState(req, res) {
    // check user role
    const userRole = req.user.role;
    const userId = req.user._id;
    if (userRole !== 'DRIVER') {
        return res.status(400).json({'message': 'Operation not allowed for current user'})
    }
    try {
        // get active load
        const activeLoad = await Load.findOne({assigned_to: userId});

        if (!activeLoad) {
            return res.json({'message': 'No active load found'})
        }
        // check if state is 'Arrived to delivery'
        const loadState = activeLoad.state;

        let newLoadState = '';

        const index = loadStates.findIndex(state => state === loadState);
        // change load state to next
        if (index >= 0 && index < loadStates.length) {
            newLoadState = loadStates[index + 1];

            if (newLoadState === loadStates[3]) {
                // change load status to 'SHIPPED' and clear its assigned_to field
                const shippedLoad = await activeLoad.updateOne({
                    $set: {
                        state: newLoadState, 
                        status: 'SHIPPED',
                        assigned_to: null
                    },
                    $push: {
                        logs: {
                            'message': `Load state updated to ${newLoadState}`
                        }
                    }
                })
                // change truck status to 'IS
                const activeTruck = await Truck.findOne({assigned_to: userId});
    
                if (activeTruck) {
                    const unloadedTruck = await activeTruck.updateOne({
                        $set: {
                            status: truckStates.inServise
                        }
                    })
                }
                return res.status(200).json({'message': `Load state changed to ${newLoadState}`});
            }
            // change in db
            const updatedLoad = await activeLoad.updateOne({
                $set: {
                    state: newLoadState
                },
                $push: {
                    logs: {
                        'message': `Load state updated to ${newLoadState}`
                    }
                }
            });
            res.status(200).json({'message': `Load state changed to ${newLoadState}`});
        }

    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

async function getShippingInfo(req, res) {
    const loadId = req.params.id;

    if (!loadId) {
        return res.status(400).json({'message': 'No id specified'})
    }

    const userRole = req.user.role;

    if (userRole !== 'SHIPPER') {
        return res.status(400).json({'message': 'Operation not allowed for current user'})
    }

    try {
        const foundLoad = await Load.findOne({_id: loadId});
        const driverId = foundLoad.assigned_to;
        const foundTruck = await Truck.findOne({assigned_to: driverId});
        
        res.status(200).json({
            'load': foundLoad,
            'truck': foundTruck
        })
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

module.exports = {
    createLoad,
    getLoads,
    updateNewLoads,
    deleteNewLoads,
    postLoad,
    getActiveLoad,
    changeLoadState,
    getShippingInfo
}