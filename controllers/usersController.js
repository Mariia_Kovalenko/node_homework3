const User = require('../models/User');
const bcrypt = require('bcryptjs');
const Truck = require('../models/Truck');
const {truckStates} = require('../config/constants');

async function getUser(req, res) {
    try {
        const user = await User.findById({ _id: req.user._id });
        if (!user) {
            return res.status(400).json({'message': 'User not found'});
        }
        const {_id, name, role, email, createdDate, assigned_load } = user;
        res.status(200).json({'user': {_id, name, role, email, createdDate, assigned_load}});
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

async function deleteUser(req, res) {
    try {
        const deletedUser = await User.findByIdAndDelete({ _id: req.user._id });
        if (!deletedUser) {
            return res.status(400).json({'message': 'User not found'});
        }
        res.status(200).json({'message': 'Profile deleted successfully'});
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

async function updateUser(req, res) {
    try {
        const userRole = req.user.role;
        if (userRole === 'DRIVER') {
            // check if there is any driver`s truck that`s on load
            const activeTruck = await Truck.findOne({assigned_to: req.user._id, status: truckStates.onLoad});
            if (activeTruck) {
                return res.status(400).json({'message': 'You cannot change any profile info while you are on load'})
            }
        }

        const {oldPassword, newPassword} = req.body;
        const user = await User.findById({ _id: req.user._id });
        const match = await bcrypt.compare(oldPassword, user.password);
        // console.log('match', match);
        // hash password
        const hashedPass = await bcrypt.hash(newPassword, 10);

        const updatedUser = await User.findByIdAndUpdate(req.user._id, {password: hashedPass});
        if (!updatedUser) {
            return res.status(400).json({'message': 'not found'});
        }
        res.status(200).json({'message': 'Password changed successfully'});
    } catch (error) {
        res.status(500).json({'message': error.message});
    }
}

module.exports = {
    getUser,
    deleteUser,
    updateUser
}