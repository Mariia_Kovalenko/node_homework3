const Truck = require('../models/Truck');
const {truckValidation} = require('../config/truckValidation');
const {truckStates} = require('../config/constants');

async function createTruck(req, res) {
    // data validation
    const {error} = truckValidation(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const {type, status} = req.body;
    const userId = req.user._id;

    // check user role
    const userRole = req.user.role;
    if (userRole !== 'DRIVER') {
        return res.status(400).json({"message": "Operation not allowed for current user"});
    }

    const truck = new Truck({
        created_by: userId,
        type: type,
        status: status
    })
    try {
        const newTruck = await truck.save();
        res.status(200).json({"message": "Truck created successfully"});
    } catch (error) {
        res.status(500).json({"message": error.message});
    }
}

async function getTrucks(req, res) {
    try {
        // check user role
        const userRole = req.user.role;
        if (userRole !== 'DRIVER') {
            return res.status(400).json({"message": "Operation not allowed for current user"});
        }

        const userId = req.user._id;
        const trucks = await Truck.find({created_by: userId});

        res.status(200).json({"trucks": trucks});
    } catch (error) {
        res.status(500).json({"message": error.message});
    }
}

async function getTruckById(req, res) {
    try {
        const truckId = req.params.id;
        const userId = req.user._id;
        const userRole = req.user.role;

        if (userRole !== 'DRIVER') {
            return res.status(400).json({"message": "Operation not allowed for current user"});
        }

        if (!truckId) {
            return res.status(400).json({"message": "No id specified"});
        }

        const truck = await Truck.findOne({created_by: userId, _id: truckId});
        res.status(200).json({"truck": truck});
    } catch (error) {
        res.status(500).json({"message": error.message});
    }
}

async function deleteTruckById(req, res) {
    try {
        const truckId = req.params.id;
        const userId = req.user._id;
        const userRole = req.user.role;

        if (userRole !== 'DRIVER') {
            return res.status(400).json({"message": "Operation not allowed for current user"});
        }

        if (!truckId) {
            return res.status(400).json({"message": "No id specified"});
        }

        // check if truck exists and is assigned to current user
        const truck = await Truck.findOne({_id: truckId});
        if (!truck) {
            return res.status(400).json({"message": "No truck with such id found"});
        }

        if (truck.assigned_to?.toString() === userId) {
            return res.status(400).json({"message": "Cannot delete assigned truck"});
        }

        const deleteTruck = await Truck.findByIdAndDelete({created_by: userId, _id: truckId});
        res.status(200).json({"message": "Truck deleted successfully"});
    } catch (error) {
        res.status(500).json({"message": error.message});
    }
}

async function updateTruckById(req, res) {
    try {
        // data validation
        const {error} = truckValidation(req.body);
        if (error) {
            return res.status(400).send(error.details[0].message);
        }
        const userRole = req.user.role;

        if (userRole !== 'DRIVER') {
            return res.status(400).json({"message": "Operation not allowed for current user"});
        }

        const { type } = req.body;
        const truckId = req.params.id;
        const userId = req.user._id;

        if (!truckId) {
            return res.status(400).json({"message": "No id specified"});
        }

        // check if there is any driver`s truck that`s on load
        const activeTruck = await Truck.findOne({assigned_to: userId, status: truckStates.onLoad});
        if (activeTruck) {
            return res.status(400).json({"message": "You cannot change any truck info while you are on load"});
        }

        // check if truck exists and is assigned to current user
        const truck = await Truck.findOne({_id: truckId});
        if (!truck) {
            return res.status(400).json({"message": "No truck with such id found"});
        } 
        
        if (truck.assigned_to?.toString() === userId) {
            return res.status(400).json({"message": "Cannot update assigned truck info"});
        }

        const updatedTruck = await Truck.findByIdAndUpdate(truckId, {type: type});
        res.status(200).json({"message": "Truck details changed successfully"});
    } catch (error) {
        res.status(500).json({"message": error.message});
    }
}

async function assignTruck(req, res) {
    const truckId = req.params.id;
    const userId = req.user._id;

    if (!truckId) {
        return res.status(400).json({"message": "No id specified"});
    }

    const userRole = req.user.role;
    if (userRole !== 'DRIVER') {
        return res.status(400).json({"message": "Operation not allowed for current user"});
    }

    try {
        // check if truck exists in db
        const truck = await Truck.findOne({_id: truckId});
        if (!truck) {
            return res.status(400).json({"message": "No truck with such id found"});
        }

        // ckeck if there is any truck already assigned to this driver
        const assignedTruck = await Truck.findOne({assigned_to: userId});
        if (assignedTruck) {
            return res.status(400).json({"message": "You already have assigned truck"});
        }

        const updatedTruck = await Truck.findByIdAndUpdate(truckId, {assigned_to: userId});
        res.status(200).json({"message": "Truck assigned successfully"});
    } catch (error) {
        res.status(500).json({"message": error.message});
    }
}    

module.exports = {
    createTruck,
    getTrucks,
    getTruckById,
    deleteTruckById,
    updateTruckById,
    assignTruck,
}